﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIO
{
    class Functions
    {
        public void writeToFile(Data data)
        {
            StreamWriter sw = new StreamWriter(data.fileName, true);
            if(data.gender == true)
            {
                sw.WriteLine("Name: " + data.name + "   Age: " + data.age + "   Birthdate: " + data.birthday + "   Gender: Male");
            }
            else
            {
                sw.WriteLine("Name: " + data.name + "   Age: " + data.age + "   Birthdate: " + data.birthday + "   Gender: Female");
            }

            sw.Close();
        }
        public string[] readFromFile(Data data)
        {
            string[] readFile = new string[50];
            //do stuff here to get the strings.
            int i = 0;
            foreach (string line in File.ReadAllLines(data.fileName))
            {
                if(line != "")
                {
                    readFile[i] = line;
                    i++;
                }
            }
            return readFile;
        }
    }
}
