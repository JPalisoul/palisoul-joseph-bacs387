﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace FileIO
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public void buttonFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            of.ShowDialog();
            fileName.Text = of.FileName;
        }

        public void buttonWrite_Click(object sender, RoutedEventArgs e)
        {
            Data data = new Data();
            data.name = nameInput.Text;
            data.age = Convert.ToInt32(ageInput.Text);
            data.birthday = DateTime.Parse(birthdateInput.Text);    
            data.gender = genderInput.Text == "1" ? true : false;
            data.fileName = fileName.Text;

            Functions function = new Functions();
            function.writeToFile(data);
        }

        public void buttonRead_Click(object sender, RoutedEventArgs e)
        {
            Functions function = new Functions();
            Data readData = new Data();
            readData.fileName = fileName.Text;
            string[] printArray = function.readFromFile(readData);
            listOutput.Items.Clear();
            string comBookString = "";
            for (int i = 0; i < 50; i++)
            {
                comBookString = (printArray[i]);
                listOutput.Items.Add(printArray[i]);
                comBookString = "";
            }
        }
    }
}
